package model;


import java.util.Observable;

import client.Client;


public class MyModel extends Observable implements Model
{
	private Problem problem;
	private Solution solution;
	
	public MyModel()//constructor
	{
		problem = new Problem();
	}

	@Override
	public void selectDomain(String domainArgs) //selects a domain and creates it by getting domain's arguments
	{
		problem.setDomainArgs(domainArgs);
	}
	@Override
	public void selectAlgorithm(String algorithmName) 
	{
		problem.setAlgorithmName(algorithmName);
	}
	@Override
	public Solution getSolution() //gets the solution of the domain, if there is one
	{
		return solution; 
	}
	@Override
	public void solveDomain() 
	{
		Client client = new Client();
		solution = client.getSolution(problem);
		
		this.setChanged();
		this.notifyObservers();
	}
}
