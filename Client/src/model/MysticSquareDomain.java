package model;
//import java.lang.reflect.Array;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class MysticSquareDomain implements SearchDomain,Serializable
{
	int rows,columns;//amount of rows and columns
	MysticSquareState start;//start state
	MysticSquareState goal;//goal state
	ArrayList<MysticSquareState> possibleStates; 
	Action[] actions;//array of all possible actions
	
	public MysticSquareDomain(int rows,int columns)//default constructor
	{
		this.rows=rows;
		this.columns=columns;
		generateGoalState(rows, columns);
		generateActions();
		this.start=randomState(rows,columns);
		possibleStates=new ArrayList<MysticSquareState>();
	}
	public MysticSquareDomain(MysticSquareDomain msd)//constructor
	{
		this.rows=msd.rows;
		this.columns=msd.columns;
		this.start=new MysticSquareState(msd.start);
		this.goal=new MysticSquareState(msd.goal);
		generateActions();
		possibleStates=new ArrayList<MysticSquareState>();
	}
	public MysticSquareDomain(String start)//constructor
	{
		generateStartState(start);
		initiateRowsAndColumns(start);
		generateGoalState(rows,columns);
		generateActions();
		possibleStates=new ArrayList<MysticSquareState>();
	}
	public MysticSquareDomain(MysticSquareState start)//constructor
	{	
		this.rows=start.getRows();
		this.columns=start.getColumns();
		this.start=start;
		generateGoalState(rows, columns);
		generateActions();
		possibleStates=new ArrayList<MysticSquareState>();
	}	
	public MysticSquareDomain(int[][] puzzle)
	{
		this.rows=puzzle.length;
		this.columns=puzzle[0].length;
		generateGoalState(rows, columns);
		generateActions();
		this.start=new MysticSquareState(puzzle, 0);
		possibleStates=new ArrayList<MysticSquareState>();
	}
	private void initiateRowsAndColumns(String start)
	{
		String[] splited=start.split(",");
		this.rows=Integer.parseInt(splited[0]);
		this.columns=Integer.parseInt(splited[1]);
	}
	private void generateStartState(String start)//generates start state
	{
		this.start=new MysticSquareState(start, 0);
	}
	private void generateGoalState(int rows,int columns)//generates goal state
	{
		String g=rows+","+columns;
		for (int i = 1; i <rows*columns; i++) 
		{
			g+=","+i;
		}
		g+=",0";
		this.goal=new MysticSquareState(g, 0);
	}
	private void generateActions()//initiate the possible actions
	{
		actions=new Action[4];
		actions[0]=new Action("up",1);
		actions[1]=new Action("right",1);
		actions[2]=new Action("down",1);
		actions[3]=new Action("left",1);
	}
	/*private MysticSquareState randomState(int rows,int columns)//randomize a state
	{
		Random rnd= new Random();
		String randomState=rows+","+columns;
		ArrayList<Integer> als=generateRandomList(0, rows*columns-1);
		while(!als.isEmpty())
		{
			int index=rnd.nextInt(als.size());
			randomState+=","+als.get(index);
			als.remove(index);
		}
		return new MysticSquareState(randomState, 0);
	}*/
	private MysticSquareState randomState(int rows,int columns)//randomize a state
	{
		Random rnd= new Random();
		MysticSquareState temp=new MysticSquareState(goal);
		for (int i = 1; i <=50; i++) 
		{
			int a=rnd.nextInt(4);
			while(!temp.move(actions[a]))
				a=rnd.nextInt(4);
		}
		return new MysticSquareState(temp);
	}
	private ArrayList<Integer> generateRandomList(int start,int end)
	{
		ArrayList<Integer> als=new ArrayList<Integer>();
		for (int i = start; i <=end; i++) 
		{	
			als.add(i);
		}
		return als;
	}
	@Override
	public State getStartState()
	{
		return start;
	}
	@Override
	public State getGoalState() 
	{
		return goal;
	}
	@Override
	public HashMap<Action, State> getAllPossibleMoves(State current) 
	{
		HashMap<Action, State> possibleMoves=new HashMap<Action, State>();
		for (int i = 0; i < actions.length; i++) 
		{
			MysticSquareState temp=((MysticSquareState)current).getANewStateByMove(actions[i]);
			if(temp!=null)
			{
				addState(temp);
				temp=possibleStates.get(possibleStates.indexOf(temp));
				possibleMoves.put(actions[i], temp);
			}
		}
		return possibleMoves;
	}
	public void showStatesByActions(ArrayList<Action> al)//shows every state after every action it got at 
	{
		int k=1;
		MysticSquareState temp=start;
		System.out.println(k+" - \n"+temp);
		for(Action a:al)
		{
			k++;
			System.out.println(k+" - \n"+temp.move(a));	
		}
	}
	@Override
	public String getProblemDescription()
	{
		String str=rows+","+columns;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++) 
			{
				str+=","+(start.getPuzzle())[i][j];
			}
		}
		return str;
	}
	@Override
	public String toString()//toString method
	{
		return start.toString();
	}
	private void addState(MysticSquareState state)
	{
		if(!possibleStates.contains(state))
			possibleStates.add(state);
	}
}
