package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Solution implements Serializable
{
	private ArrayList<Action> actions;//the actions required to solve the domain's problem
	private String problemDescription;//the problem's description
	public Solution(ArrayList<Action> actions)//constructor
	{
		setActions(actions);
	}
	public ArrayList<Action> getActions()//returns the actions required to solve the domain's problem by an array list
	{
		return actions;
	}
	public void setActions(ArrayList<Action> actions) //sets the actions required to solve the domain's problem
	{
		this.actions=actions;
	}
	public String getProblemDescription()//gets problem's description
	{
		return problemDescription;
	}
	public void setProblemDescription(String problemDescription)//sets problem's description
	{
		this.problemDescription = problemDescription;
	}
}
