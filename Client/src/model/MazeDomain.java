package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

public class MazeDomain implements SearchDomain,Serializable
{
	int rows,columns,walls;//amount of rows,columns and walls
	MazeState start;//start state
	MazeState goal;//goal state
	MazeState[][] mazeStateArray;//array that represents a maze
	Action[] actions;//array of all possible actions
	
	public MazeDomain(int rows,int columns,int walls)//constructor
	{
		this.rows=rows;
		this.columns=columns;
		this.walls=walls;
		this.mazeStateArray=new MazeState[rows][columns];
		initialization(walls);
		generateActions();
	}
	public MazeDomain(SearchDomain other)//constructor
	{
		MazeDomain mOther=(MazeDomain)other;
		this.rows=mOther.rows;
		this.columns=mOther.columns;
		this.start=mOther.start;
		this.goal=mOther.goal;
		this.mazeStateArray=new MazeState[rows][columns];
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				this.mazeStateArray[i][j]=new MazeState(mOther.mazeStateArray[i][j]);
			}
		}
	}
	/*public MazeDomain(String args)
	{
		String[] split=args.split(",");
		this.rows=Integer.parseInt(split[0]);
		this.columns=Integer.parseInt(split[1]);
		this.walls=Integer.parseInt(split[2]);
		this.mazeStateArray=new MazeState[rows][columns];
		initialization(walls);
		generateActions();
	}*/
	public MazeDomain(String args)
	{
		String[] split=args.split(",");
		this.rows=Integer.parseInt(split[0]);
		this.columns=Integer.parseInt(split[1]);
		this.mazeStateArray=new MazeState[rows][columns];
		generateActions();
		int k=2;
		this.walls=0;
		for (int i = 0; i < mazeStateArray.length; i++) 
		{
			for (int j = 0; j < mazeStateArray[0].length; j++) 
			{
				String indexStr=((Integer)(k-1)).toString();
				mazeStateArray[i][j]=new MazeState(indexStr, i, j);
				if(split[k]=="1")
				{
					mazeStateArray[i][j].block();
					this.walls++;
				}
				k++;
			}
		}
		int startRow=Integer.parseInt(split[k]);
		k++;
		int startColumn=Integer.parseInt(split[k]);
		this.start=mazeStateArray[startRow][startColumn];
		generateGoalState();
	}
	public MazeDomain(int[][] arr,int startRow,int startColumn)
	{
		this.rows=arr.length;
		this.columns=arr[0].length;
		mazeStateArray=new MazeState[rows][columns];
		int index=1;
		this.walls=0;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				String indexStr=((Integer)index).toString();
				mazeStateArray[i][j]=new MazeState(indexStr, i, j);
				if(arr[i][j]==1)
				{
					mazeStateArray[i][j].block();
					this.walls++;
				}
				index++;
			}
		}
		this.start=mazeStateArray[startRow][startColumn];
		generateGoalState();
		generateActions();
	}
	
	public String getProblemDescription()
	{
		String problemDescription=rows+","+columns;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				int b=0;
				if(mazeStateArray[i][j].isBlocked())
					b=1;
				problemDescription+=","+b;
			}
		}
		problemDescription+=","+start.getRow()+","+start.getColumn();
		return problemDescription;
	}
	@Override
	public State getStartState()//returns start state
	{
		return start;
	}
	@Override
	public State getGoalState()//returns goal state
	{
		return goal;
	}
	public void setStartState(int row,int column)
	{
		if(row>0 && row<rows && column>0 && column<columns)
			this.start=mazeStateArray[row][column];
	}
	private void initialization(int walls)//initiate the maze domain by getting amount of walls
	{
		int index=1;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				String indexStr=((Integer)index).toString();
				mazeStateArray[i][j]=new MazeState(indexStr, i, j);
				index++;
			}
		}
		generateStartState();
		do
		{
			generateGoalState();
		}while(goal.equals(start));
		generateWalls(walls);
		/*while(!isPossible(mazeStateArray))
		{
			generateWalls(walls);
		}*/
	}
	private void generateStartState()//randomize and initiates the start state
	{
		//Random rnd=new Random();
		//int rndR=rnd.nextInt(rows);
		//int rndC=rnd.nextInt(columns);
		int rndR=0;
		int rndC=0;
		start=mazeStateArray[rndR][rndC];
	}
	private void generateGoalState()//randomize and initiates the goal state
	{
		//Random rnd=new Random();
		//int rndR=rnd.nextInt(rows);
		//int rndC=rnd.nextInt(columns);
		int rndR=rows-1;
		int rndC=columns-1;
		goal=mazeStateArray[rndR][rndC];
	}
	private void generateWalls(int walls)//randomize walls for the maze
	{
		Random rnd=new Random();
		for (int i = 1; i <=walls; i++) 
		{
			int rndR=rnd.nextInt(rows);
			int rndC=rnd.nextInt(columns);
			while(mazeStateArray[rndR][rndC].equals(start)||mazeStateArray[rndR][rndC].equals(goal)||mazeStateArray[rndR][rndC].isBlocked())
			{
				rndR=rnd.nextInt(rows);
				rndC=rnd.nextInt(columns);
			}
			mazeStateArray[rndR][rndC].block();
		}
	}
	private void generateActions()//initiate the possible actions
	{
		actions=new Action[4];
		actions[0]=new Action("up",1);
		actions[1]=new Action("right",1);
		actions[2]=new Action("down",1);
		actions[3]=new Action("left",1);
	}
	@Override
	public HashMap<Action, State> getAllPossibleMoves(State current)//returns hash map of all the possible moves for a current state
	{
		HashMap<Action,State> possibleMoves=new HashMap<Action,State>();
		int currentRow=((MazeState)current).getRow();
		int currentColumn=((MazeState)current).getColumn();
		if(currentRow-1>=0)
			if(!mazeStateArray[currentRow-1][currentColumn].isBlocked())
				possibleMoves.put(actions[0],mazeStateArray[currentRow-1][currentColumn]);
		if(currentColumn+1<columns)
			if(!mazeStateArray[currentRow][currentColumn+1].isBlocked())
				possibleMoves.put(actions[1], mazeStateArray[currentRow][currentColumn+1]);
		if(currentRow+1<rows)
			if(!mazeStateArray[currentRow+1][currentColumn].isBlocked())
				possibleMoves.put(actions[2],mazeStateArray[currentRow+1][currentColumn]);
		if(currentColumn-1>0)
			if(!mazeStateArray[currentRow][currentColumn-1].isBlocked())
				possibleMoves.put(actions[3], mazeStateArray[currentRow][currentColumn-1]);
		return possibleMoves;
	}
	public int[][] getMazeByIntArray()
	{
		return toIntArray(this.mazeStateArray);
	}
	private int[][] toIntArray(MazeState[][] mazeArray)
	{
		int[][] arr=new int[rows][columns];
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				if(mazeArray[i][j].isBlocked())
					arr[i][j]=1;
				else
				{
					arr[i][j]=0;	
				}
			}
		}
		return arr;
	}
	/*private boolean isPossible(MazeState[][] mazeArray)
	{
		int[][] arr=toIntArray(mazeArray);
		return find(start.getRow(),start.getColumn(),goal.getRow(),goal.getColumn(),arr);
	}
	private boolean isLegal(int row, int column, int[][] a)
    {
        return ((row>=0&&row<a.length)&&(column>=0&&column<a[0].length));
    }
	private boolean find(int startRow, int startColumn, int goalRow, int goalColumn, int[][] a)
    {
		a[startRow][ startColumn] = 2;
        if (a[startRow][ startColumn] == a[goalRow][ goalColumn])
            return true;
        if (isLegal(startRow - 1, startColumn, a) &&a[startRow-1][startColumn]==0&& find(startRow - 1, startColumn, goalRow, goalColumn, a))
            return true;
        if (isLegal(startRow, startColumn + 1, a) && a[startRow][ startColumn+1] == 0 && find(startRow, startColumn + 1, goalRow, goalColumn, a))
            return true;
        if (isLegal(startRow + 1, startColumn, a) && a[startRow + 1][ startColumn] == 0 && find(startRow + 1, startColumn, goalRow, goalColumn, a))
            return true;
        if (isLegal(startRow, startColumn - 1, a) && a[startRow][ startColumn-1] == 0 && find(startRow, startColumn - 1, goalRow, goalColumn, a))
            return true;
        a[startRow][ startColumn] = 0;
        return false;
    }*/
	@Override
	public String toString()//toString method
	{
		String str="\n";
		for (int i = 0; i <rows ; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				if(!mazeStateArray[i][j].equals(start)&&!mazeStateArray[i][j].equals(goal))
					str+=mazeStateArray[i][j];
				else
				{
					if(mazeStateArray[i][j].equals(start))
						str+="(s)";
					else
						str+="(g)";
				}
			}
			str+='\n';
		}
		return str;
	}
}
