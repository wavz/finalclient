package model;

import java.io.Serializable;

public class MysticSquareState extends State implements Serializable
{

	private int rows, columns;//rows and columns of state's puzzle
	private int[][] puzzle;//state's puzzle
	public MysticSquareState(String state, double cost) 
	{
		super(state, cost);
		generatePuzzle(state);
	}
	public MysticSquareState(MysticSquareState other)
	{
		super(other.getState(),other.getCost());
		generatePuzzle(other.getState());
	}
	public MysticSquareState(int[][] state,double cost)
	{
		super("",cost);
		super.setState(this.puzzleToState(state, state.length, state[0].length));
		this.rows=state.length;
		this.columns=state[0].length;
		this.puzzle=new int[rows][columns];
		for (int i = 0; i < puzzle.length; i++) 
		{
			for (int j = 0; j < state[0].length; j++) 
			{
				puzzle[i][j]=state[i][j];
			}
		}
	}
	
	public boolean move(Action a)//get an action, if it fits to the possible actions, changes the puzzle by the move, and returns true, else, returns false
	{
		switch(a.getAction())
		{
			case "up":return moveUp();
			case "right":return moveRight();
			case "down":return moveDown();
			case "left":return moveLeft();
			default:return false;
		}
	}
	public MysticSquareState getANewStateByMove(Action a)//get an action and returns a stated created by launching the action on the current state
	{
		MysticSquareState s=new MysticSquareState(this);
		if(!s.move(a))
			return null;
		return s;
	}
	private boolean moveUp()//launches the action 'up' on the state
	{	
		if(zeroRow()==rows-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR+1][zeroC];
		puzzle[zeroR+1][zeroC]=0;
		updateState();
		return true;
	}
	private boolean moveRight()//launches the action 'right' on the state
	{
		if(zeroColumn()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC-1];
		puzzle[zeroR][zeroC-1]=0;
		updateState();
		return true;
	}
	private boolean moveDown()//launches the action 'down' on the state
	{
		if(zeroRow()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR-1][zeroC];
		puzzle[zeroR-1][zeroC]=0;
		updateState();
		return true;
	}
	private boolean moveLeft()//launches the action 'left' on the state
	{
		if(zeroColumn()==columns-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC+1];
		puzzle[zeroR][zeroC+1]=0;
		updateState();
		return true;
	}
	private int zeroRow()//returns zero's row number in the array
	{
		for(int i=0;i<rows;i++)
		{
			for(int j=0;j<columns;j++)
			{
				if(puzzle[i][j]==0)
					return i;
			}
		}
		return -1;
	}
	private int zeroColumn()//returns zero's column number in the array
	{
		for(int i=0;i<rows;i++)
		{
			for(int j=0;j<columns;j++)
			{
				if(puzzle[i][j]==0)
					return j;
			}
		}
		return -1;
	}
	private void updateState()//updates the state
	{
		this.setState(puzzleToState(puzzle, rows, columns));
	}
	private String puzzleToState(int[][] puzzle,int rows,int columns)//returns a string description by puzzle
	{
		String state=rows+","+columns;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				state+=","+puzzle[i][j];
			}
		}
		return state;
	}
	public void generatePuzzle(String args) //generates the puzzle by arguments
	{
		String[] splited = args.split(",");
		this.rows = Integer.parseInt(splited[0]);
		this.columns = Integer.parseInt(splited[1]);
		this.puzzle = new int[rows][columns];
		int k = 2;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
			{
				puzzle[i][j] = Integer.parseInt(splited[k]);
				k++;
			}
	}
	public int getRows() //gets the rows
	{
		return rows;
	}
	public void setRows(int rows)//sets the rows
	{
		this.rows = rows;
	}
	public int getColumns() //returns the columns
	{
		return columns;
	}
	public void setColumns(int columns) //sets the columns
	{
		this.columns = columns;
	}
	public int[][] getPuzzle() //returns the puzzle
	{
		return puzzle;
	}
	@Override
	public String toString()//toString method
	{
		String s="\n";
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				s+="["+puzzle[i][j]+"]";
			}
			s+="\n";
		}
		return s;
	}
}
