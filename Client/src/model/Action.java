package model;

import java.io.Serializable;

public class Action implements Serializable
{
	String action;//description of action
	double price;//price of action
		
		public Action(String description) //constructor
		{
			this.action=description;
		}
		public Action(String description,double price)//constructor
		{
			this.action=description;
			this.price=price;
		}
		@Override 
		public boolean equals(Object obj)//equals method
		{
			return action.equals(((Action)obj).action);
		}
		@Override
		public int hashCode()//hash code function (uses the string class hash code function)
		{
			return action.hashCode();
		}	
		@Override
		public String toString()//toString method
		{
			return action;
		}
		public double getPrice()//returns the price of the action
		{
			return price;
		}
		public void setPrice(double price)//sets the price of the action
		{
			this.price=price;
		}
		public String getAction()//returns action's description
		{
			return action;
		}
		public void setAction(String action)//sets action's description
		{
			this.action = action;
		}
}
