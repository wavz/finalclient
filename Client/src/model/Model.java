package model;

import java.io.Serializable;
import java.util.Observer;

import org.eclipse.swt.events.SelectionListener;

import tasks.Task;

public interface Model extends Serializable
{
	void selectDomain(String domainArgs);//selects a domain by getting domain's name
	void selectAlgorithm(String algorithmName);//selects algorithm by arguments 
	Solution getSolution();//achieves the solution of the domain, if there is one
	void addObserver(Observer o);//adds observer
	void solveDomain();
}
