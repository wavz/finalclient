package model;

import java.io.Serializable;

public abstract class State implements Comparable<State>, Serializable
{
	private String state;//state's description
    private double cost;//state's cost  
    private State cameFrom;//the state that led to the current state
    private Action cameWithAction;//the action that led to the current state
    
    public State(String state,double cost)//constructor
    {        
        this.state = state;
        this.cost = cost;
        this.cameFrom = null;
        this.cameWithAction=null;
    }
    @Override
    public boolean equals(Object obj)//equals method
    {
        return state.equals(((State)obj).state);
    }
	public String getState()//returns state's description
	{
		return state;
	}
	public void setState(String state)//sets state's description
	{
		this.state = state;
	}
	public double getCost()//returns state's cost
	{
		return cost;
	}
	public void setCost(double cost)//sets state's cost
	{
		this.cost = cost;
	}
	public State getCameFrom()//returns the state that led to the current state
	{
		return cameFrom;
	}
	public void setCameFrom(State cameFrom)//sets the state that led to the current state
	{
		this.cameFrom = cameFrom;
	}
	public Action getCameWithAction()//returns the action that led to the current state
	{
		return cameWithAction;
	}
	public void setCameWithAction(Action cameWithAction)//sets the action that led to the current state
	{
		this.cameWithAction = cameWithAction;
	} 
	@Override
	public int compareTo(State other)//compareTo method
	{
		if(this.equals(other))
			return 0;
		if(this.cost>other.cost)
			return 1;
		return -1;
	}
}
