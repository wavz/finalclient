package presenter;

import java.util.HashMap;

import tasks.TaskRunnable;
import model.Model;
import model.MyModel;

public class UserCommands
{
	private HashMap<String, Command> commands =new HashMap<String,Command>();//holds a transformation from a command's description to a command creator
	public UserCommands()//constructor
	{
		commands.put("SelectDomain", new SelectDomainCommand());
		commands.put("SelectAlgorithm", new SelectAlgorithmCommand());
		commands.put("SolveDomain", new SolveDomainCommand());
	}	
	/*public void doCommand(Model model, String commandName, String args)//doing a command by getting model, the command name and the required arguments for the command
	{
		Command command = commands.get(commandName);
		if (command != null)
		{
			command.doCommand(model, args);
		}
	}	*/
	public Command selectCommand(String commandName)//gets command's name and returns the command
	{
		Command command = commands.get(commandName);
		return command;
	}
	public interface Command//an interface of a command
	{
		Model doCommand(Model model, String args);
	}	
	private class SelectDomainCommand implements Command//a select domain command
	{
		@Override
		public Model doCommand(Model model, String args)
		{
			Model m = new MyModel();
			m.selectDomain(args);	
			return m;
		}
	}
	private class SelectAlgorithmCommand implements Command//a select algorithm command
	{
		@Override
		public Model doCommand(Model model, String args)
		{
			model.selectAlgorithm(args);
			return model;	
		}			
	}	
	private class SolveDomainCommand implements Command//a solve domain command
	{
		@Override
		public Model doCommand(Model model, String args) 
		{
			model.solveDomain();	
			return model;
		}
	}
}

