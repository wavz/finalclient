package presenter;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import presenter.UserCommands.Command;
import view.MainWindow;
import view.MazeWindow;
import view.MyConsoleView;
import view.PuzzleWindow;
import view.View;
import model.Model;
import model.MyModel;
import model.MysticSquareDomain;
import model.MysticSquareState;
import model.Solution;

public class Presenter implements Observer
{
	private Model model;// observable member that holds the model
	private View view;// observable member that holds the view
	private UserCommands commands;// the commander
	private ArrayList<Model> models; // all running models
	public Presenter(Model model, View view)// constructor
	{
		this.model = model;
		this.view = view;
		commands = new UserCommands();
		this.models=new ArrayList<Model>();
	}

	@Override
	public void update(Observable observable, Object arg1) // updates if there are changes
	{
		if (observable instanceof Model) 
		{
			Solution solution = ((Model) observable).getSolution();
			view.displaySolution(solution);
		}
		else if (observable == view)
		{
			String action = view.getUserAction();
			String[] arr = action.split(" ");
			String commandName = arr[0];
			String args = null;
			if (arr.length > 1)
				args = arr[1];
			Command command = commands.selectCommand(commandName);
			Model m = command.doCommand(model, args);
			// Check if we got a new model from the command
			if (m != model)
			{
				this.model = m;
				models.add(m);
				m.addObserver(this);
			}
		}
	}
    public void setView(View view)
    {
    	this.view=view;
    }
	
    public static void printArray(int[][] arr)
    {
    	for (int i = 0; i < arr.length; i++) 
    	{
    		for (int j = 0; j < arr[0].length; j++) 
    		{
    			System.out.print("["+arr[i][j]+"]");
			}
    		System.out.println();
		}
    }
 
    /*public static void main(String[] args)
    {
    	int[][] mazeData={
				{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,1,1,0,1,0,0,1},
				{0,0,1,1,1,1,1,0,0,1,0,1,0,1,1},
				{1,1,1,0,0,0,1,0,1,1,0,1,0,0,1},
				{1,0,1,0,1,1,1,0,0,0,0,1,1,0,1},
				{1,1,0,0,0,1,0,0,1,1,1,1,0,0,1},
				{1,0,0,1,0,0,1,0,0,0,0,1,0,1,1},
				{1,0,1,1,0,1,1,0,1,1,0,0,0,1,1},
				{1,0,0,0,0,0,0,0,0,1,0,1,0,0,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,0,1,0},
			};
    	//mazeData=generator.generate("10,10,50");
    	printArray(mazeData);
    	MyModel model = new MyModel();
		MyConsoleView view = new MyConsoleView();
		Presenter presenter = new Presenter(model, view);
		model.addObserver(presenter);
		view.addObserver(presenter);
		//view.start();
    	MazeWindow mw=new MazeWindow(presenter, 700, 700, "Maze",mazeData);
    	mw.run();
    }*/
    /*public static void main(String[] args)
	{
		MyModel model = new MyModel();
		MyConsoleView view = new MyConsoleView();
		Presenter presenter = new Presenter(model, view);
		model.addObserver(presenter);
		view.addObserver(presenter);
		System.out.println("lmnop6");
		view.start();
		System.out.println("lmnop7");
	}*/
    /*public static void main(String[] args)
    {
    	String str = "7,0,2,8,3,6,5,1,4";
    	int [][] puzzle;
    	MysticSquareDomain msd=new MysticSquareDomain(3, 3);
    	puzzle =((MysticSquareState)msd.getStartState()).getPuzzle();
    	printArray(puzzle);
    	MyModel model = new MyModel();
		MyConsoleView view = new MyConsoleView();
		Presenter presenter = new Presenter(model, view);
		model.addObserver(presenter);
		view.addObserver(presenter);
		PuzzleWindow pw= new PuzzleWindow(presenter,700,700,"8Puzzle",puzzle);
		pw.start();
    }*/
    public static void main(String[] args)
    {
    	MyModel model = new MyModel();
		MyConsoleView view = new MyConsoleView();
		Presenter presenter = new Presenter(model, view);
		model.addObserver(presenter);
		view.addObserver(presenter);
    	MainWindow mw=new MainWindow(presenter, 750, 430, "Welcome to our project :)");
    	mw.run();
    }
}
