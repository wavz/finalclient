package presenter;

import java.util.ArrayList;
/**
 * A class that allows us to manage the threads.
 * this is a Singelton pattern because we dont want more than one threads manager
 * @author itay&chen 
 *
 */
public class ThreadsManager 
{
	private static ThreadsManager instance=null;//the instance of the threads manager
	private ArrayList<Thread> threads=null;//array list of all program's threads 
	private ThreadsManager()//default constructor
	{
		this.threads=new ArrayList<Thread>();
	}
	/**
	 * 
	 * @return returns the singleton instance of threads manager
	 */
	public static ThreadsManager getInstance()//returns the singleton instance of threads manager
	{
		if(instance==null)
		{
			instance=new ThreadsManager();
		}
		return instance;
	}
	/**
	 * adding a thread to the array
	 * @param t
	 */
	public void addThread(Thread t)//adds a thread to the threads' array list
	{
		if(!threads.contains(t))
			threads.add(t);
	}
	/**
	 * closing all threads
	 */
	public void closeAllThreads()//closes all program's threads
	{
		for(Thread t:threads)
			t.interrupt();
	}
}
