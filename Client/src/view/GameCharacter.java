package view;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

class GameCharacter
{
	   int x,y;
	   int row,column;
	   public GameCharacter(int x,int y)
	   {
		   this.x=x;
		   this.y=y;
	   }
	   public GameCharacter()
	   {
		   setToStart();
	   }
	   
	   public void paint(PaintEvent e,int w,int h)
	   {
		  /* e.gc.setForeground(new Color(null,0,0,255));
		   e.gc.setBackground(new Color(null, 0, 255, 0));
		   
		   e.gc.fillOval(x,y, w, h);		
		   e.gc.drawOval(x,y, w, h);*/
		   Image img = new Image(null,"resources/bak.jpg");
		   e.gc.drawImage(img, 0, 0, img.getBounds().width, img.getBounds().height, x, y, w, h);
	   }
	   
	   public int getX() 
	   {
		   return x;
	   }
	   public int getY() 
	   {
		   return y;
	   }
	   public int getRow() 
	   {
		   return row;
	   }
	   public int getColumn() 
	   {
		   return column;
	   }
	   public void setX(int x)
	   {
		   this.x=x;
	   }
	   public void setY(int y)
	   {
		   this.y=y;
	   }	   
	   public void setRow(int row)
	   {
		   this.row=row;
	   }
	   public void setColumn(int column)
	   {   
		   this.column=column;
	   }
	
	   public void setToStart()
	   {
		   x=0;
		   y=0;
		   row=0;
		   column=0;
	   }
	   
	   public void setLocationByArray(int[][] arr,int width,int height)
	   {
		   setXByArray(arr, width);
		   setYByArray(arr, height);
	   }
	   private void setXByArray(int[][] arr,int width)
	   {
		   this.x=column*(width/arr[0].length);
	   }
	   private void setYByArray(int[][] arr,int height)
	   {
		   this.y=row*(height/arr.length);
	   }
	   
	   public boolean moveUp(int[][] arr,int width,int height)
	   {
		   if(row>0&&y>=height/arr.length)
			   if(arr[row-1][column]==0)
			   {
				   row--;
				   y-=height/arr.length;
				   return true;
			   }
		   return false;
	   }
	   public boolean moveRight(int[][] arr,int width,int height)
	   {
		   if(column<arr[0].length-1&&x+width/arr[0].length<=width)
			   if(arr[row][column+1]==0)
			   {
				   column++;
				   x+=width/arr[0].length;
				   return true;
			   }
		   return false;
	   }
	   public boolean moveDown(int[][] arr,int width,int height)
	   {
		   if(row<arr.length-1&&y+height/arr.length<=height)
			   if(arr[row+1][column]==0)
			   {
				   row++;
				   y+=height/arr.length;
				   return true;
			   }
		   return false;
	   }
	   public boolean moveLeft(int[][] arr,int width,int height)
	   {
		   if(column>0&&x>=width/arr[0].length)
			   if(arr[row][column-1]==0)
			   {
				   column--;
				   x-=width/arr[0].length;
				   return true;
			   }
		   return false;
	   }
}

