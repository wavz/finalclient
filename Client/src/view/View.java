package view;

import tasks.Task;
import model.Solution;

public interface View extends Task
{
	public void start();//start the view of the program
	public void displayCurrentState();//displays the current state
	public void displaySolution(Solution solution);//displays the domain's solution
	public String getUserAction();//gets user actions
}
