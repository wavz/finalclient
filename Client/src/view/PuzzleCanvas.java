package view;


import model.Action;
import model.MysticSquareDomain;
import model.MysticSquareState;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
/**
 * This is the Board of the Puzzle
 * @author itay&chen
 *
 */
public class PuzzleCanvas extends Canvas
{
	
	int[][] puzzle;
	int [][] original;
	Image[] images;
	/**
	 * Constructor for the canvas puzzle
	 * @param parent
	 * @param style
	 * @param arr 
	 */
	
	public PuzzleCanvas(Composite parent, int style,int[][]arr)
	
	{
		super(parent, style);
		setPuzzle(arr);
		
		
		imagesInitialization();

		//set a white background (red,green,blue)
		setBackground(new Color(null,255,255,255));
		
		addPaintListener(new PaintListener() 
		{	
			@Override
			public void paintControl(PaintEvent e) 
			{
				e.gc.setForeground(new Color(null,0,0,0));
				e.gc.setBackground(new Color(null,0,0,0));
				
				int width=getSize().x;
				int height=getSize().y;

				int w=width/puzzle.length;
				int h=height/puzzle[0].length;
				 
				for(int i=0;i<puzzle.length;i++)
					for(int j=0;j<puzzle[0].length;j++)
					{
					        //int x=j*w;
					        //int y=i*h;    
						Image numImage=images[puzzle[i][j]];
						e.gc.drawImage(numImage, 0, 0, numImage.getBounds().width, numImage.getBounds().height, j*w, i*h, w, h);
					}
			}
		});
		setOriginal(puzzle);
		addDisposeListener(new DisposeListener() 

		{
			@Override
			public void widgetDisposed(DisposeEvent arg0)
			{
			}
		});
		
		addKeyListener(new KeyListener() 
		{
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyPressed(KeyEvent e)
			{
				getDisplay().syncExec(new Runnable() 
				{
					@Override
					public void run() 
					{
						int width=getSize().x;
						int height=getSize().y;
						switch (e.keyCode) 
						{
						case SWT.ARROW_UP:
						{
							moveUp();
							redraw();
						}
						break;
						case SWT.ARROW_RIGHT:
						{
							moveRight();
							redraw();
						}
						break;
						case SWT.ARROW_DOWN:
						{
							moveDown();
							redraw();
						}
						break;
						case SWT.ARROW_LEFT:
						{
							moveLeft();
							redraw();
						}
						break;
						}
						
					}
				});
			}
		});
	}
	/**
	 * creating the canvas puzzle with the numbers
	 */
	public void start()
	{
		getDisplay().syncExec(new Runnable()
		{
			@Override
			public void run() 
			{
				setPuzzle(original);
				
				redraw();
			}
		});
		
	}
	
	/**
	 * setting the puzzle from the constructor
	 * @param arr
	 */
	
	public void setPuzzle(int[][] arr)
	{
		puzzle=new int[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++) 
		{
			for (int j = 0; j < arr[0].length; j++) 
			{
				puzzle[i][j]=arr[i][j];
			}
		}
	}
	/**
	 * initializing the images into the array
	 */
	public void imagesInitialization()
	{
		images=new Image[9];
		Image num0 = new Image(null,"resources/button0.png");
		images[0]=num0;
		Image num1 = new Image(null,"resources/button1.png");
		images[1]=num1;
		Image num2 = new Image(null,"resources/button2.png");
		images[2]=num2;
		Image num3 = new Image(null,"resources/button3.png");
		images[3]=num3;
		Image num4 = new Image(null,"resources/button4.png");
		images[4]=num4;
		Image num5 = new Image(null,"resources/button5.png");
		images[5]=num5;
		Image num6 = new Image(null,"resources/button6.png");
		images[6]=num6;
		Image num7 = new Image(null,"resources/button7.png");
		images[7]=num7;
		Image num8 = new Image(null,"resources/button8.png");
		images[8]=num8;
		
	}
	/**
	 * this function is for the hint button 
	 * @param a
	 * @return
	 */
	public boolean move(Action a)//get an action, if it fits to the possible actions, changes the puzzle by the move, and returns true, else, returns false
	{
		switch(a.getAction())
		{
			case "up":return moveUp();
			case "right":return moveRight();
			case "down":return moveDown();
			case "left":return moveLeft();
			default:return false;
		}
	}
	/**
	 * move up command
	 * @return true when the player moves up
	 */
	public boolean moveUp()//launches the action 'up' on the state

	{
		if(zeroRow()==puzzle.length-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR+1][zeroC];
		puzzle[zeroR+1][zeroC]=0;

		getDisplay().syncExec(new Runnable()
		{	
			@Override
			public void run() 
			{
				redraw();
			}
		});
		winDetector();
		return true;
	}
	/**
	 * move down command
	 * @return true when the player moves down 
	 */
	public boolean moveDown() 
	{
		if(zeroRow()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR-1][zeroC];
		puzzle[zeroR-1][zeroC]=0;
		getDisplay().syncExec(new Runnable()
		{	
			@Override
			public void run() 
			{
				redraw();
			}
		});

		winDetector();
		return true;
	}
	/**
	 * move left command
	 * @return true when the player moves left
	 */
	public boolean moveLeft()
	{
		if(zeroColumn()==puzzle[0].length-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC+1];
		puzzle[zeroR][zeroC+1]=0;
		getDisplay().syncExec(new Runnable()
		{	
			@Override
			public void run() 
			{
				redraw();
			}
		});
		winDetector();
		return true;
	}
	/**
	 * move right command
	 * @return true when the player moves right
	 */
	public boolean moveRight()
	{
		if(zeroColumn()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC-1];
		puzzle[zeroR][zeroC-1]=0;
		getDisplay().syncExec(new Runnable()
		{	
			@Override
			public void run() 
			{
				redraw();
			}
		});
		winDetector();
		return true;
	}
	/**
	 * 
	 * @return the zero's row number in the array
	 */

	public int zeroRow()//returns zero's row number in the array
	{
		for(int i=0;i<puzzle.length;i++)
		{
			for(int j=0;j<puzzle[0].length;j++)
			{
				if(puzzle[i][j]==0)
					return i;
			}
		}
		return -1;
	}
	/**
	 * 
	 * @return the zero's column number in the array
	 */
	public int zeroColumn()//returns zero's column number in the array
	{
		for(int i=0;i<puzzle.length;i++)
		{
			for(int j=0;j<puzzle[0].length;j++)
			{
				if(puzzle[i][j]==0)
					return j;
			}
		}
		return -1;
	}
	/**
	 * this is a method that checks every move whether if the player won or not
	 * @return true when the player won and open a massage box
	 */
	
	public void setOriginal(int[][]arr)
	{
		original = new int[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++) 
		{
			for (int j = 0; j < arr[0].length; j++) 
			{
				original[i][j]=arr[i][j];
			}
			
		}
	}
	public void reset(int[][]arr)
	{
		getDisplay().syncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				setPuzzle(arr);	
				setOriginal(arr);
				redraw();
			}
		});
	}
	
	public void setCanvasByMysticPuzzle(MysticSquareDomain msd)
	{
		int[][] arr=((MysticSquareState)msd.getStartState()).getPuzzle();
		setOriginal(arr);
		setPuzzle(arr);
	}
	public boolean winDetector()
	{
		int[][] goal={{1,2,3},{4,5,6},{7,8,0}};
		for (int i = 0; i < puzzle.length; i++) 
		{
			for (int j = 0; j < puzzle[0].length; j++) 
			{
				if(puzzle[i][j]!=goal[i][j])
					return false;
			}
		}
		Shell shell = new Shell(getDisplay());
		MessageBox messageBox = new MessageBox(shell, SWT.YES);
	    messageBox.setMessage("You won!");
	    int rc = messageBox.open();
	    return true;
	}
	public int[][] getPuzzle() {
		return puzzle;
	}
	public boolean click(int x,int y)
	{
		int row=rowByPixels(y);
		int column=columnByPixels(x);
		return moveByClick(row, column);
		
	}
	private int rowByPixels(int y)
	{
		int height=getSize().y;
		return (y*puzzle.length)/(height+1);
	}
	private int columnByPixels(int x)
	{
		int width=getSize().x;
		return (x*puzzle[0].length)/(width+1);
	}
	private boolean moveByClick(int row,int column)
	{
		if(puzzle[row][column]==0)
			return false;
		if(row>0)
			if(puzzle[row-1][column]==0)
				return moveUp();
		if(column<puzzle[0].length-1)
			if(puzzle[row][column+1]==0)
				return moveRight();
		if(row<puzzle.length-1)
			if(puzzle[row+1][column]==0)
				return moveDown();
		if(column>0)
			if(puzzle[row][column-1]==0)
				return moveLeft();
		return false;
		
	}
}
