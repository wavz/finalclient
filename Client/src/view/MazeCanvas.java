package view;

import java.security.MessageDigestSpi;

import model.Action;
import model.MazeDomain;
import model.MazeState;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;


public class MazeCanvas extends Canvas
{

	int[][] maze;
	GameCharacter character;
	boolean flag=true;
	public MazeCanvas(Composite parent, int style,int[][] mazeData)
	{
		super(parent, style);
		Image wall = new Image(null,"resources/Random square wall 002.jpg");
		Image egg = new Image(null,"resources/gol.png"); 
		generateMaze(mazeData);
		
		character = new GameCharacter();
		
		//set a white background (red,green,blue)
		setBackground(new Color(null,255,255,255));
		

		addPaintListener(new PaintListener() 
		{
			@Override
			public void paintControl(PaintEvent e) 
			{
				 e.gc.setForeground(new Color(null,0,0,0));
				 e.gc.setBackground(new Color(null,0,0,0));

				 int width=getSize().x;
				 int height=getSize().y;

				 int w=width/maze[0].length;
				 int h=height/maze.length;

				 for(int i=0;i<maze.length;i++)
				    for(int j=0;j<maze[i].length;j++)
				    {
				        int x=j*w;
				        int y=i*h;
				        if(maze[i][j]!=0)
				           e.gc.fillRectangle(x,y,w,h);
				        	//e.gc.drawImage(wall, 0, 0, wall.getBounds().width, wall.getBounds().height, x, y, w, h);
				    }
				 if(flag)
					 character.setLocationByArray(maze, width, height);
				 character.paint(e, w, h);
				 e.gc.drawImage(egg, 0, 0, egg.getBounds().width, egg.getBounds().height, (maze[0].length-1)*w, (maze.length-1)*h, w, h);
				 flag=true;
			}	
		});
		addDisposeListener(new DisposeListener() 
		{
			@Override
			public void widgetDisposed(DisposeEvent arg0)
			{
			}
		});		
		
		addKeyListener(new KeyListener() 
		{
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyPressed(KeyEvent e)
			{
				getDisplay().syncExec(new Runnable() 
				{
					@Override
					public void run() 
					{
						int width=getSize().x;
						int height=getSize().y;
						switch (e.keyCode) 
						{
						case SWT.ARROW_UP:
						{
							character.moveUp(maze, width, height);
							redraw();
						}
						break;
						case SWT.ARROW_RIGHT:
						{
							character.moveRight(maze, width, height);
							redraw();
						}
						break;
						case SWT.ARROW_DOWN:
						{
							character.moveDown(maze, width, height);
							redraw();
						}
						break;
						case SWT.ARROW_LEFT:
						{
							character.moveLeft(maze, width, height);
							redraw();
						}
						break;
						}
						boolean b1=(character.getRow()==maze.length-1);
						boolean b2=(character.getColumn()==maze[0].length-1);
						if(b1&&b2)
						{
							MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_ERROR);
						    messageBox.setMessage("You won! :)");
						    int rc = messageBox.open();
						}
					}
				});
			}
		});
	}
	public void start()
	{
		getDisplay().syncExec(new Runnable()
		{
			@Override
			public void run() 
			{
				character.setToStart();
				flag=false;
				redraw();
			}
		});
	}
	public void reset(int[][] arr)
	{
		generateMaze(arr);
		start();
	}
	
	public void generateMaze(int[][] arr)
	{
		this.maze=new int[arr.length][arr[0].length];
		for (int i = 0; i < maze.length; i++) 
		{
			for (int j = 0; j < maze[0].length; j++) 
			{
				this.maze[i][j]=arr[i][j];
			}
		}
		this.character=new GameCharacter();
	}
	public boolean move(Action a)//get an action, if it fits to the possible actions, changes the puzzle by the move, and returns true, else, returns false
	{
		switch(a.getAction())
		{
			case "up":return moveUp();
			case "right":return moveRight();
			case "down":return moveDown();
			case "left":return moveLeft();
			default:return false;
		}
	}
	public boolean moveUp()
    {
    	boolean flag= character.moveUp(maze, getSize().x, getSize().y);
    	/*getDisplay().syncExec(new Runnable()
    	{	
			@Override
			public void run() 
			{
				redraw();
			}
		});*/
    	return flag;
    }
    public boolean moveRight()
    {
    	boolean flag= character.moveRight(maze, getSize().x, getSize().y);
    	/*getDisplay().syncExec(new Runnable()
    	{	
			@Override
			public void run() 
			{
				redraw();
			}
		});*/
    	return flag;
    }
    public boolean moveDown()
    {
    	boolean flag= character.moveDown(maze, getSize().x, getSize().y);
    	/*getDisplay().syncExec(new Runnable()
    	{	
			@Override
			public void run() 
			{
				redraw();
			}
		});*/
    	return flag;
    }
    public boolean moveLeft()
    {
    	boolean flag= character.moveLeft(maze, getSize().x, getSize().y);
    	/*getDisplay().syncExec(new Runnable()
    	{	
			@Override
			public void run() 
			{
				redraw();
			}
		});*/
    	return flag;
    }
    public int getProblemStartRow()
    {
    	return character.getRow();
    }
    public int getProblemStartColumn()
    {
    	return character.getColumn();
    }
    
    public void setCanvasByMazeDomain(MazeDomain md)
    {
    	this.maze=md.getMazeByIntArray();
    	character.setRow(((MazeState)md.getStartState()).getRow());
    	character.setColumn(((MazeState)md.getStartState()).getColumn());
    }
    public boolean click(int x,int y)
	{
		int row=rowByPixels(y);
		int column=columnByPixels(x);
		return fillByClick(row, column);
	}
    public boolean doubleClick(int x,int y)
	{
		int row=rowByPixels(y);
		int column=columnByPixels(x);
		return unfillByDoubleClick(row, column);
	}
	private int rowByPixels(int y)
	{
		int height=getSize().y;
		return (y*maze.length)/(height+1);
	}
	private int columnByPixels(int x)
	{
		int width=getSize().x;
		return (x*maze[0].length)/(width+1);
	}
	private boolean fillByClick(int row,int column)
	{
		boolean startCheck=(row==0&&column==0);
		boolean goalCheck=(row==maze.length-1 && column==maze[0].length-1);
		if(startCheck||goalCheck)
			return false;
		maze[row][column]=1;
		return true;
	}
	private boolean unfillByDoubleClick(int row,int column)
	{
		boolean startCheck=(row==0&&column==0);
		boolean goalCheck=(row==maze.length-1 && column==maze[0].length-1);
		if(startCheck||goalCheck)
			return false;
		maze[row][column]=0;
		return true;
	}
}
