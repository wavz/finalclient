package view;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;

import javax.print.attribute.standard.Sides;

import model.Action;
import model.MazeDomain;
import model.MysticSquareDomain;
import model.MysticSquareState;
import model.Problem;
import model.Solution;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

import client.Client;
import presenter.Presenter;
/**
 * this is the class for the main puzzle window
 * @author itay&chen
 *
 */
public class PuzzleWindow extends BasicWindow 
{
	int[][] puzzle;
	Problem problem;
	MysticSquareDomain puzzlEight;
	Menu menuBar, fileMenu, helpMenu;

	MenuItem fileMenuHeader, helpMenuHeader;

	MenuItem fileExitItem, fileSaveItem, helpGetHelpItem;

	/**
	 * constructor for the window
	 * @param presenter our presenter
	 * @param width the width of the whole window
	 * @param height the height of the whole window
	 * @param title the title of the window
	 * @param arr
	 */

	public PuzzleWindow(Presenter presenter, int width, int height, String title,int[][]arr) 
	{
		super(presenter, width, height, title);
		puzzlEight=new MysticSquareDomain(arr);
		this.puzzle=new int[arr.length][arr[0].length];
		for (int i = 0; i < puzzle.length; i++) 
		{
			for (int j = 0; j < puzzle[0].length; j++) 
			{
				this.puzzle[i][j]=arr[i][j];
			}
		}
		problem=new Problem();
		problem.setAlgorithmName("Astar");
		problem.setDomainArgs(puzzlEight.getProblemDescription());
	}
	public PuzzleWindow(Presenter presenter, int width, int height, String title,int[][]arr,Display display) 
	{
		super(presenter, width, height, title,display);
		puzzlEight=new MysticSquareDomain(arr);
		this.puzzle=new int[arr.length][arr[0].length];
		for (int i = 0; i < puzzle.length; i++) 
		{
			for (int j = 0; j < puzzle[0].length; j++) 
			{
				this.puzzle[i][j]=arr[i][j];
			}
		}
		problem=new Problem();
		problem.setAlgorithmName("Astar");
		problem.setDomainArgs(puzzlEight.getProblemDescription());
	}
	/**
	 * this methods creates the structure of the window(like adding buttons/images/labels etc.) 
	 */
	@Override
	void initWidgets()
	{
		menuBar = new Menu(shell, SWT.BAR);
	    fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
	    fileMenuHeader.setText("&File");
	    
	    fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);
		

	    fileSaveItem = new MenuItem(fileMenu, SWT.PUSH);
	    fileSaveItem.setText("&Save");

	    fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
	    fileExitItem.setText("E&xit");

	    helpMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
	    helpMenuHeader.setText("&Help");

	    helpMenu = new Menu(shell, SWT.DROP_DOWN);
	    helpMenuHeader.setMenu(helpMenu);

	    helpGetHelpItem = new MenuItem(helpMenu, SWT.PUSH);
	    helpGetHelpItem.setText("&Get Help");
	    
		shell.setLayout(new GridLayout(2, false));
		Button start = new Button(shell, SWT.PUSH);
		start.setText("Start");
		start.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		
	    PuzzleCanvas puzzleCanvas=new PuzzleCanvas(shell, SWT.BORDER,puzzle);//we need to change this
		puzzleCanvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 9));
		
		Button hint = new Button(shell, SWT.PUSH);
		hint.setText("Give me a Hint");
		hint.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		Button reset = new Button(shell, SWT.PUSH);
		reset.setText("Reset");
		reset.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		final Combo algorithms = new Combo(shell, SWT.READ_ONLY);
		algorithms.setToolTipText("Algorithms");
		algorithms.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));//put a ♥
	    String items[] = {"Choose an algorithm..","Astar","BreadthFirstSearch","BFS", "DFS" };
	    algorithms.setItems(items);
	    algorithms.setText(algorithms.getItem(0));
		
	    Button displaySolution = new Button(shell, SWT.PUSH);
	    displaySolution.setText("Display Solution");
	    displaySolution.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
	    
	    Text textBox=new Text(shell, SWT.MULTI);
	    textBox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
	    textBox.setVisible(false);

	    Button saveGame = new Button(shell, SWT.PUSH);
		saveGame.setText("Save Game");
		saveGame.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
		
		Button loadGame = new Button(shell, SWT.PUSH);
		loadGame.setText("Load Game");
		loadGame.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
	    
		Button back = new Button(shell, SWT.PUSH);
		back.setText("Back to Main");
		back.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
		
		saveGame();
		puzzleCanvas.forceFocus();
		
		shell.addDisposeListener(new DisposeListener()
		{	
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				shell.dispose();
			}
		});
		start.addSelectionListener(new SelectionListener() 
		{	
			@Override
			public void widgetSelected(SelectionEvent e) //pu a ♥
			{
				textBox.setText("");
				textBox.setVisible(false);
				puzzleCanvas.start();
				puzzleCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		hint.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				setPuzzle(puzzleCanvas.puzzle);
				problem.setDomainArgs("MysticSquare:"+puzzlEight.getProblemDescription());
				Client client = new Client();
				System.out.println(5);
				Solution solution = client.getSolution(problem);
				System.out.println(6);
				ArrayList<Action> actions=solution.getActions();
				System.out.println(7);
				if(actions!=null)
				{
					System.out.println("the moves:");
					printArrayList(actions);
					System.out.println("the one move:");
					puzzleCanvas.move(actions.get(0));
					System.out.println(actions.get(0).getAction());
					shell.getDisplay().syncExec(new Runnable()
					{	
						@Override
						public void run() 
						{
							shell.redraw();
						}
					});
				}
				else
				{
					MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
				    messageBox.setMessage("No solution, try reset again!");
				    int rc = messageBox.open();
				}
				puzzleCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		reset.addSelectionListener(new SelectionListener()
		{	

			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				textBox.setText("");
				textBox.setVisible(false);
				MysticSquareDomain ms = new MysticSquareDomain(puzzle.length,puzzle[0].length);
				int [][]arr =  ((MysticSquareState)ms.getStartState()).getPuzzle();
				setPuzzle(arr);
				puzzleCanvas.reset(arr);
				puzzleCanvas.forceFocus();
			}	
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
		back.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				shell.getDisplay().syncExec(new Runnable()
				{
					@Override
					public void run() 
					{
						shell.dispose();//we need to check if this is the right way to exit
					}
				});
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		displaySolution.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				setPuzzle(puzzleCanvas.puzzle);
				problem.setDomainArgs("MysticSquare:"+puzzlEight.getProblemDescription());
				Client client = new Client();
				Solution solution = client.getSolution(problem);
				ArrayList<Action> actions=solution.getActions();
				if(actions!=null)
					{
					 shell.getDisplay().syncExec(new Runnable()
						{	
							@Override
							public void run() 
							{
								textBox.setVisible(true);
								int k=1;
								String moves="";
								for(Action a:actions)
								{
									moves+="Step "+k+"-("+a.getAction()+")\n";
									k++;
								}
								textBox.setText(moves);
								shell.redraw();
							}
						});
					
					}
								
								
				
				
				
				
				/*setPuzzle(puzzleCanvas.puzzle);
				problem.setDomainArgs("MysticSquare:"+puzzleight.getProblemDescription());
				Client client = new Client();
				Solution solution = client.getSolution(problem);
				ArrayList<Action> actions=solution.getActions();
				if(actions!=null)
					{
					 shell.getDisplay().syncExec(new Runnable()
						{	
							@Override
							public void run() 
							{
								for(Action a:actions)
								{
									
									
									try 
									{
										puzzleCanvas.move(a);
										Thread.sleep(1000);
										shell.redraw();
										
									} catch (InterruptedException e){}
								}
							}
						});
					}
					else
					{
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
						messageBox.setMessage("No solution, try to reset again!");
						int rc = messageBox.open();
					}*/
				

				
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
				// TODO Auto-generated method stub
				
			
		});
		saveGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				puzzle=puzzleCanvas.getPuzzle();
				puzzlEight=new MysticSquareDomain(puzzle);
				saveGame();
				puzzleCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		loadGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				textBox.setVisible(false);
				textBox.setText("");
				loadGame();
				puzzleCanvas.setCanvasByMysticPuzzle(puzzlEight);
				puzzleCanvas.redraw();
				puzzleCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		algorithms.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				if(algorithms.getText().equals(algorithms.getItem(0)))
					problem.setAlgorithmName(algorithms.getItem(1));
				else
					problem.setAlgorithmName(algorithms.getText());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{
			}
		});
		puzzleCanvas.addMouseListener(new MouseListener()
		{	
			@Override
			public void mouseUp(MouseEvent e){}
			@Override
			public void mouseDown(MouseEvent e) 
			{
				puzzleCanvas.click(e.x, e.y);
			}
			@Override
			public void mouseDoubleClick(MouseEvent e) {}
		});
	}
		
		
	/**
	 * opens the window
	 */
	
	public void printArrayList(ArrayList<Action> al)
	{
		for(Action a:al)
		{
			System.out.println(a.getAction());
		}
	}
	public void setPuzzle(int [][]arr)
	{
		this.puzzle = new int[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 0; j < arr[0].length; j++) 
			{
				this.puzzle[i][j]=arr[i][j];	
			}
		}
		puzzlEight=new MysticSquareDomain(arr);
	}
	public boolean isEqual(int [][]arr1,int [][]arr2)
	{
		for (int i = 0; i < arr1.length; i++) 
		{
			for (int j = 0; j < arr1[0].length; j++) 
			{
				if(arr1[i][j]==arr2[i][j])
					return true;
				else
					return false;
			}
			
		}
		return false;
	}
	public void saveGame()
	{
		ObjectOutputStream out=null;
		try 
		{
			out=new ObjectOutputStream(new FileOutputStream("saves/savePuzzle.dat"));
			out.writeObject(puzzlEight);
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			out.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void loadGame()
	{
		ObjectInputStream in=null;
		try 
		{
			in=new ObjectInputStream(new FileInputStream("saves/savePuzzle.dat"));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			this.puzzlEight=(MysticSquareDomain)in.readObject();
			this.puzzle=((MysticSquareState)puzzlEight.getStartState()).getPuzzle();
			problem.setDomainArgs("MysticSquare:"+puzzlEight.getProblemDescription());
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			in.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}

