package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import presenter.Presenter;

public class MainWindow extends BasicWindow
{
	public MainWindow(Presenter presenter, int width, int height, String title) 
	{
		super(presenter, width, height, title);
	}
	@Override
	public void initWidgets() 
	{
		Image newMazeGameImage = new Image(null,"resources/newMazeGame.png");
		//Image loadMazegame = new Image(null,"src/loadMazeGame.png");
		Image newPuzzleGameImage=new Image(null,"resources/newPuzzleGame.png");
		//Image loadPuzzleGameImage=new Image(null,"src/loadPuzzleGame.png");
		
		shell.setLayout(new GridLayout(2, false));
		
		Menu menuBar = new Menu(shell, SWT.BAR);
		
        MenuItem cascadeFileMenu = new MenuItem(menuBar, SWT.CASCADE);
        cascadeFileMenu.setText("&File");
        
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        cascadeFileMenu.setMenu(fileMenu);

        MenuItem propertiesItem = new MenuItem(fileMenu, SWT.PUSH);
        propertiesItem.setText("&Properties");
        shell.setMenuBar(menuBar);
        
        MenuItem exitItem = new MenuItem(fileMenu, SWT.PUSH);
        exitItem.setText("&Exit");
        shell.setMenuBar(menuBar);
        
		Button newMazeGame = new Button(shell, SWT.PUSH);
		newMazeGame.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		newMazeGame.setImage(newMazeGameImage);		
		
		/*Button loadMazeGame = new Button(shell, SWT.PUSH);
		loadMazeGame.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		loadMazeGame.setImage(loadMazegame);*/
		
		Button newPuzzleGame = new Button(shell, SWT.PUSH);
		newPuzzleGame.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		newPuzzleGame.setImage(newPuzzleGameImage);
		
		/*Button loadPuzzleGame = new Button(shell, SWT.PUSH);
		loadPuzzleGame.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		loadPuzzleGame.setImage(loadPuzzleGameImage);*/
		
		newMazeGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				int[][] mazeData={
						{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
						{0,0,0,0,0,0,0,0,1,1,0,1,0,0,1},
						{0,0,1,1,1,1,1,0,0,1,0,1,0,1,1},
						{1,1,1,0,0,0,1,0,1,1,0,1,0,0,1},
						{1,0,1,0,1,1,1,0,0,0,0,1,1,0,1},
						{1,1,0,0,0,1,0,0,1,1,1,1,0,0,1},
						{1,0,0,1,0,0,1,0,0,0,0,1,0,1,1},
						{1,0,1,1,0,1,1,0,1,1,0,0,0,1,1},
						{1,0,0,0,0,0,0,0,0,1,0,1,0,0,0},
						{1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},};
				MazeWindow mw=new MazeWindow(presenter, 700, 700, "Maze", mazeData,display);
				mw.run();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		/*loadMazeGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				int[][] mazeData={
						{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
						{0,0,0,0,0,0,0,0,1,1,0,1,0,0,1},
						{0,0,1,1,1,1,1,0,0,1,0,1,0,1,1},
						{1,1,1,0,0,0,1,0,1,1,0,1,0,0,1},
						{1,0,1,0,1,1,1,0,0,0,0,1,1,0,1},
						{1,1,0,0,0,1,0,0,1,1,1,1,0,0,1},
						{1,0,0,1,0,0,1,0,0,0,0,1,0,1,1},
						{1,0,1,1,0,1,1,0,1,1,0,0,0,1,1},
						{1,0,0,0,0,0,0,0,0,1,0,1,0,0,0},
						{1,1,1,1,1,1,1,1,1,1,1,1,0,1,0},};
				MazeWindow mw=new MazeWindow(presenter, 700, 700, "Maze", mazeData,display);
				mw.run();
				mw.loadGame();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});*/
		newPuzzleGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				int[][] arr={{7,0,2},{8,3,6},{5,1,4}};

				PuzzleWindow pw=new PuzzleWindow(presenter, 700, 700, "Puzzle Eight", arr, display);
				pw.run();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		/*loadPuzzleGame.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				int[][] arr={{7,0,2},{8,3,6},{5,1,4}};
				PuzzleWindow pw=new PuzzleWindow(presenter, 700, 700, "Puzzle Eight", arr, display);
				pw.run();
				pw.loadGame();
				pw.shell.redraw();
			}			
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});*/
		exitItem.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				shell.getDisplay().dispose();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		propertiesItem.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
		        fd.setText("Open");
		        fd.setFilterPath("resources/");
		        String[] filterExt = { "*.xml", "*.*" };
		        fd.setFilterExtensions(filterExt);
		        String selected = fd.open();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
	}
}
