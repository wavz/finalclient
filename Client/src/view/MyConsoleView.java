package view;

import java.util.Observable;
import java.util.Scanner;

import presenter.ThreadsManager;
import model.Action;
import model.Solution;


public class MyConsoleView extends Observable implements View {

	private String action;
	
	@Override
	public void start() //starts the view operation
	{
		System.out.println("Welcome to my project");
		System.out.println("Here is an example of commands that you can write:");
		System.out.println("SelectDomain Maze:20,20,200");
		System.out.println("SelectDomain MysticSquare:3,3,1,2,3,4,5,6,7,8,0");
		System.out.println("SelectAlgorithm BFS");
		System.out.println("SelectAlgorithm Astar;Maze");
		System.out.println("SolveDomain");
		System.out.println("SelectAlgorithm Astar;MysticSquare");
		System.out.println("SelectAlgorithm Dijkstra");
		System.out.println("exit");
		action = "";
		Scanner scanner = new Scanner(System.in);
		do
		{
			
			System.out.print("Enter command: ");
			action = scanner.nextLine();
			
			/*Thread t = new Thread(new TaskRunnable(this));
			ThreadsManager.getInstance().addThread(t);
			t.start();*/
			
			if (!(action.equals("exit")))
			{
				this.setChanged();
				this.notifyObservers();
			}
			
		} while (!(action.equals("exit")));
		scanner.close();
		ThreadsManager.getInstance().closeAllThreads();
	}
	@Override
	public void displayCurrentState()//displays the current state
	{
		
	}
	@Override
	public void displaySolution(Solution solution)//displays the solution
	{
		
		if(solution.getActions()==null)
			System.out.println("no solution");
		else
		{
			int k=1;
			for(Action a : solution.getActions())
				System.out.println("("+(k++)+") "+a);
		}
	}
	@Override
	public String getUserAction()//gets user's action
	{		
		return action;
	}
	@Override
	public void doTask() //operate the function start(), used for thread's operation
	{
		this.start();
	}
}
