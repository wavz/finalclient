package view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

import client.Client;
import presenter.Presenter;
import model.Action;
import model.MazeDomain;
import model.Problem;
import model.Solution;

//public class MazeWindow extends UIView
public class MazeWindow extends BasicWindow
{
	int[][] maze;
	Problem problem;
	MazeDomain mazeDomain;
	int rows,columns,walls;
	public MazeWindow(Presenter presenter, int width, int height, String title,int[][] maze) 
	{
		super(presenter, width, height, title);
		setMaze(maze);
		rcwInitialization(maze.length, maze[0].length,wallsByIntArray(maze));
		problem=new Problem();
		problem.setAlgorithmName("BFS");
		problem.setDomainArgs(mazeDomain.getProblemDescription());
		System.out.println("the maze:\n"+mazeDomain);
	}
	public MazeWindow(Presenter presenter, int width, int height, String title,int[][] maze,Display display) 
	{
		super(presenter, width, height, title,display);
		setMaze(maze);
		rcwInitialization(maze.length, maze[0].length,wallsByIntArray(maze));
		problem=new Problem();
		problem.setAlgorithmName("BFS");
		problem.setDomainArgs(mazeDomain.getProblemDescription());
		System.out.println("the maze:\n"+mazeDomain);
	}
	/*
	public MazeWindow(Presenter presenter, Display display, int width,int height, String title,int[][] maze) 
	{
		super(presenter, display, width, height, title);
		this.maze=new int[maze.length][maze[0].length];
		for (int i = 0; i < maze.length; i++) 
		{
			for (int j = 0; j < maze[0].length; j++) 
			{
				this.maze[i][j]=maze[i][j];
			}
		}
	}
*/
	
	/*@Override
	public void start()
	{	
		run();
	}*/

	/*
	@Override
	public void displayCurrentState() 
	{
		
	}

	@Override
	public void displaySolution(Solution solution)
	{
		
	}

	@Override
	public String getUserAction()
	{
		return null;
	}
*/
	@Override
	void initWidgets()
	{
		shell.setLayout(new GridLayout(2, false));
		Button start = new Button(shell, SWT.PUSH);
		start.setText("Start");
		start.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		
		final MazeCanvas mazeCanvas=new MazeCanvas(shell, SWT.BORDER,maze);//we need to change this
		mazeCanvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 13));
		
		Button hint = new Button(shell, SWT.PUSH);
		hint.setText("Hint");
		hint.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		Button reset = new Button(shell, SWT.PUSH);
		reset.setText("Reset");
		reset.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		final Combo algorithms = new Combo(shell, SWT.READ_ONLY);
		algorithms.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));//put a ♥
	    String items[] = {"Choose an algorithm..","Astar","BreadthFirstSearch","BFS", "DFS" };
	    algorithms.setItems(items);
	    algorithms.setText(algorithms.getItem(0));
	    
	    Button displaySolution = new Button(shell, SWT.PUSH);
	    displaySolution.setText("Display solution");
	    displaySolution.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
	    Text rowsTextBox=new Text(shell, SWT.SINGLE|SWT.BORDER|SWT.CENTER);
	    rowsTextBox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
	    rowsTextBox.setText("rows");
	    
	    Text columnsTextBox=new Text(shell, SWT.SINGLE|SWT.BORDER|SWT.CENTER);
	    columnsTextBox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
	    columnsTextBox.setText("columns");
	    
	    Text wallsTextBox=new Text(shell, SWT.SINGLE|SWT.BORDER|SWT.CENTER);
	    wallsTextBox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
	    wallsTextBox.setText("walls");
		
	    Text displaySolutionTextBox=new Text(shell, SWT.MULTI|SWT.BORDER);
	    displaySolutionTextBox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
	    displaySolutionTextBox.setVisible(false);
	    
	    Button edit = new Button(shell, SWT.PUSH);
		edit.setText("Edit - ON");
		edit.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
	    
		
		Button saveGame = new Button(shell, SWT.PUSH);
		saveGame.setText("Save Game");
		saveGame.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
		
		Button loadGame = new Button(shell, SWT.PUSH);
		loadGame.setText("Load Game");
		loadGame.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
		
		Button back = new Button(shell, SWT.PUSH);
		back.setText("Back");
		back.setLayoutData(new GridData(SWT.FILL, SWT.DOWN, false, false, 1, 1));
		
		mazeCanvas.forceFocus();
		saveGame();
		
		start.addSelectionListener(new SelectionListener() 
		{	
			@Override
			public void widgetSelected(SelectionEvent e) //pu a ♥
			{
				displaySolutionTextBox.setVisible(false);
				displaySolutionTextBox.setText("");
				mazeCanvas.start();
				mazeCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		hint.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				displaySolutionTextBox.setVisible(false);
				displaySolutionTextBox.setText("");
				boolean b1=(mazeCanvas.getProblemStartRow()==maze.length-1);
				boolean b2=(mazeCanvas.getProblemStartColumn()==maze[0].length-1);
				if(b1&b2)
				{
					MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
				    messageBox.setMessage("You don't need any hint, you won! :)");
				    int rc = messageBox.open();
				}
				else
				{
					setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
					problem.setDomainArgs("Maze:"+mazeDomain.getProblemDescription());
					Client client = new Client();
					Solution solution = client.getSolution(problem);
					ArrayList<Action> actions=solution.getActions();
					if(actions!=null)
					{
						mazeCanvas.move(actions.get(0));
						boolean b3=(mazeCanvas.getProblemStartRow()==maze.length-1);
						boolean b4=(mazeCanvas.getProblemStartColumn()==maze[0].length-1);
						if(b3&b4)
						{
							MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
						    messageBox.setMessage("You won! :)");
						    int rc = messageBox.open();
						}
						shell.getDisplay().syncExec(new Runnable()
						{	
							@Override
							public void run() 
							{
								mazeCanvas.redraw();
							}
						});
					}
					else
					{
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
						messageBox.setMessage("No solution, try to reset again!");
						int rc = messageBox.open();
					}
				}
			}	
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0){}
		});
		reset.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				displaySolutionTextBox.setVisible(false);
				displaySolutionTextBox.setText("");
				int[][] mazeData=generateRandomMaze();
				setMaze(mazeData);
				mazeCanvas.reset(mazeData);
				mazeCanvas.forceFocus();
			}	
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		displaySolution.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				displaySolutionTextBox.setText("");
				boolean b1=(mazeCanvas.getProblemStartRow()==maze.length-1);
				boolean b2=(mazeCanvas.getProblemStartColumn()==maze[0].length-1);
				if(b1&b2)
				{
					displaySolutionTextBox.setVisible(false);
					MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
				    messageBox.setMessage("You don't need any solution, you won! :)");
				    int rc = messageBox.open();
				}
				else
				{
					setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
					problem.setDomainArgs("Maze:"+mazeDomain.getProblemDescription());
					Client client = new Client();
					Solution solution = client.getSolution(problem);
					ArrayList<Action> actions=solution.getActions();
					if(actions!=null)
					{
						displaySolutionTextBox.setVisible(true);
						String solutionActions="";
						int k=1;
						for(Action a:actions)
						{
							solutionActions+="Step "+k+"-("+a.getAction()+")\n";
							k++;
						}
						displaySolutionTextBox.setVisible(true);
						displaySolutionTextBox.setText(solutionActions);
					}
					else
					{
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
						messageBox.setMessage("No solution, try to reset again!");
						int rc = messageBox.open();
						displaySolutionTextBox.setVisible(false);
						displaySolutionTextBox.setText("");
					}
				}
				mazeCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		back.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				shell.getDisplay().syncExec(new Runnable()
				{
					@Override
					public void run() 
					{
						shell.dispose();//we need to check if this is the right way to exit
					}
				});
				
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});	
		algorithms.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				displaySolutionTextBox.setVisible(false);
				displaySolutionTextBox.setText("");
				if(algorithms.getText().equals(algorithms.getItem(0)))
					problem.setAlgorithmName("BFS");
				else
					problem.setAlgorithmName(algorithms.getText());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});	
		saveGame.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				int r=mazeCanvas.getProblemStartRow();
				int c=mazeCanvas.getProblemStartColumn();
				mazeDomain.setStartState(r, c);
				saveGame();
				mazeCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});	
		loadGame.addSelectionListener(new SelectionListener()
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{			
				displaySolutionTextBox.setVisible(false);
				displaySolutionTextBox.setText("");
				loadGame();
				mazeCanvas.setCanvasByMazeDomain(mazeDomain);
				mazeCanvas.redraw();
				mazeCanvas.forceFocus();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		
		rowsTextBox.addModifyListener(new ModifyListener() 
		{	
			@Override
			public void modifyText(ModifyEvent e) 
			{
				boolean b1=rowsTextBox.getText().equals("rows");
				boolean b2=rowsTextBox.getText().equals("");
				if(!b1&&!b2)
				{
					int r=Integer.parseInt(rowsTextBox.getText());
					setRows(r);
				}
			}
		});
		columnsTextBox.addModifyListener(new ModifyListener() 
		{	
			@Override
			public void modifyText(ModifyEvent e) 
			{
				boolean b1=columnsTextBox.getText().equals("columns");
				boolean b2=columnsTextBox.getText().equals("");
				if(!b1&&!b2)
				{
					int c=Integer.parseInt(columnsTextBox.getText());
					setColumns(c);
				}
			}
		});
		wallsTextBox.addModifyListener(new ModifyListener() 
		{	
			@Override
			public void modifyText(ModifyEvent e) 
			{
				boolean b1=wallsTextBox.getText().equals("walls");
				boolean b2=wallsTextBox.getText().equals("");
				if(!b1)
				{
					if(!b2)
					{
						int w=Integer.parseInt(wallsTextBox.getText());
						setWalls(w);
						if(w>0.75*(rows*columns))
						{
							MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
							messageBox.setMessage("you should set less walls, less than 75% of the maze size");
							messageBox.open();
						}
					}
					else
						setWalls(0);
				}
			}
		});
		
		rowsTextBox.addMouseListener(new MouseListener()
		{	
			@Override
			public void mouseUp(MouseEvent e) {}
			@Override
			public void mouseDown(MouseEvent e) 
			{
				rowsTextBox.selectAll();
			}
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				rowsTextBox.setText("");
			}
		});
		columnsTextBox.addMouseListener(new MouseListener()
		{	
			@Override
			public void mouseUp(MouseEvent e) {}
			@Override
			public void mouseDown(MouseEvent e) 
			{
				columnsTextBox.selectAll();
			}
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				columnsTextBox.setText("");
			}
		});
		wallsTextBox.addMouseListener(new MouseListener()
		{	
			@Override
			public void mouseUp(MouseEvent e) {}
			@Override
			public void mouseDown(MouseEvent e) 
			{
				wallsTextBox.selectAll();
			}
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				wallsTextBox.setText("");
			}
		});
	
		rowsTextBox.addFocusListener(new FocusListener()
		{	
			@Override
			public void focusLost(FocusEvent e){}
			@Override
			public void focusGained(FocusEvent e) 
			{	
				rowsTextBox.selectAll();
			}
		});
		columnsTextBox.addFocusListener(new FocusListener()
		{	
			@Override
			public void focusLost(FocusEvent e){}
			@Override
			public void focusGained(FocusEvent e) 
			{	
				columnsTextBox.selectAll();
			}
		});
		wallsTextBox.addFocusListener(new FocusListener()
		{	
			@Override
			public void focusLost(FocusEvent e){}
			@Override
			public void focusGained(FocusEvent e) 
			{	
				wallsTextBox.selectAll();
			}
		});
		
		edit.addSelectionListener(new SelectionListener()
		{	
			@Override
			public void widgetSelected(SelectionEvent e)
			{	
				if(edit.getText().equals("Edit - ON"))
					edit.setText("Edit - OFF");
				else
					edit.setText("Edit - ON");
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e){}
		});
		
		mazeCanvas.addMouseListener(new MouseListener()
		{	
			@Override
			public void mouseUp(MouseEvent e){}
			@Override
			public void mouseDown(MouseEvent e)
			{

				if(edit.getText().equals("Edit - ON"))
				{
					if(e.button==1)
					{
						mazeCanvas.click(e.x, e.y);
						setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
						mazeCanvas.redraw();
					}
					else
					{
						mazeCanvas.doubleClick(e.x, e.y);
						setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
						mazeCanvas.redraw();
					}
				}
			}
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				if(edit.getText().equals("Edit - ON"))
				{
					mazeCanvas.doubleClick(e.x, e.y);
					setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
					mazeCanvas.redraw();
				}
			}
		});
		mazeCanvas.addMouseMoveListener(new MouseMoveListener()
		{	
			@Override
			public void mouseMove(MouseEvent e) 
			{	
				if(e.stateMask==524288)
				{
					mazeCanvas.click(e.x, e.y);
					setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
					mazeCanvas.redraw();
				}
				else
				{
					if(e.stateMask==2097152)
					{
						mazeCanvas.doubleClick(e.x, e.y);
						setMaze(mazeCanvas.maze,mazeCanvas.getProblemStartRow(),mazeCanvas.getProblemStartColumn());
						mazeCanvas.redraw();
					}
				}
			}
		});
	}
	public void setMaze(int[][] maze)
	{
		setMaze(maze, 0, 0);
	}
	public void setMaze(int[][] maze,int startRow,int startColumn)
	{
		this.maze=new int[maze.length][maze[0].length];
		for (int i = 0; i < maze.length; i++) 
		{
			for (int j = 0; j < maze[0].length; j++) 
			{
				this.maze[i][j]=maze[i][j];
			}
		}
		this.mazeDomain=new MazeDomain(maze,startRow,startColumn);
		this.rows=maze.length;
		this.columns=maze[0].length;
		this.walls=wallsByIntArray(maze);
	}
	public void doTask()
	{
		// TODO Auto-generated method stub
		
	}
	public void printArrayList(ArrayList<Action> al)
	{
		for(Action a:al)
		{
			System.out.println(a.getAction());
		}
	}
	private void rcwInitialization(int rows,int columns,int walls)
	{
		this.rows=rows;
		this.columns=columns;
		this.walls=walls;
	}
	private int wallsByIntArray(int[][] arr)
	{
		int count=0;
		for (int i = 0; i < arr.length; i++) 
		{
			for (int j = 0; j < arr[0].length; j++) 
			{
				count+=arr[i][j];
			}
		}
		return count;
	}
	private int[][] generateRandomMaze()
	{
		MazeDomain md=new MazeDomain(this.rows,this.columns, this.walls);
		int[][] mazeData=(md).getMazeByIntArray();
		return mazeData;
	}
	public void setRows(int rows)
	{
		if(rows>0)
			this.rows=rows;
	}
	public void setColumns(int columns)
	{
		if(columns>0)
			this.columns=columns;
	}
	public void setWalls(int walls)
	{
		if(walls>=0)
			this.walls=walls;
	}
	public void saveGame()
	{
		ObjectOutputStream out=null;
		try 
		{
			out=new ObjectOutputStream(new FileOutputStream("saves/saveMaze.dat"));
			out.writeObject(mazeDomain);
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			out.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void loadGame()
	{
		ObjectInputStream in=null;
		try 
		{
			in=new ObjectInputStream(new FileInputStream("saves/saveMaze.dat"));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			this.mazeDomain=(MazeDomain)in.readObject();
			this.maze=mazeDomain.getMazeByIntArray();
			rcwInitialization(maze.length, maze[0].length, wallsByIntArray(maze));
			problem.setDomainArgs("Maze:"+mazeDomain.getProblemDescription());
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			in.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
