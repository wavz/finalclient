package tasks;

public class TaskRunnable implements Runnable
{
	private Task t;

	public TaskRunnable(Task t)//constructor
	{
		this.t = t;
	}
	@Override
	public void run() //operate the function doTask() of the task t
	{
		t.doTask();
	}
}
