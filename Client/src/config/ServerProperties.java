package config;
import java.io.Serializable;
/**
 * a class for the server details
 * @author itay&chen
 *
 */
public class ServerProperties implements Serializable 
{
	private int port;
	private int numOfClients;
	public int getPort()
	{
		return port;
	}
	public void setPort(int port)
	{
		this.port = port;
	}
	public int getNumOfClients()
	{
		return numOfClients;
	}
	public void setNumOfClients(int numOfClients)
	{
		this.numOfClients = numOfClients;
	}
	public ServerProperties(int port, int numOfClients)
	{
		super();
		this.port = port;
		this.numOfClients = numOfClients;
	}	
	public ServerProperties() { }
}
