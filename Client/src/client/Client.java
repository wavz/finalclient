package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import model.Problem;
import model.Solution;
/**
 * this is the class for the client
 * @author wavz
 *
 */
public class Client
{
	private String serverAddress;
	private int port;
	/**
	 * constructore that creates the client
	 */
	public Client()
	{
		this.serverAddress = "127.0.0.1";
		this.port = 8000;
	}
	/**
	 * this is basically the method that allows us to  communicate with the server
	 * @param problem
	 * @return the solution for the game
	 */
	public Solution getSolution(Problem problem)
	{		
		Socket socket = null;
		ObjectInputStream in = null;
		ObjectOutputStream out = null;
		
		//System.out.println("Send new problem: " + problem.getDomainName());
		try
		{
			socket = new Socket(serverAddress, port);
			out = new ObjectOutputStream(socket.getOutputStream());
			
			out.writeObject(problem);
			in = new ObjectInputStream(socket.getInputStream());
			Solution solution = (Solution)in.readObject();
			//System.out.println("Found solution: " + solution.getProblemDescription());
			return solution;				
		}
		catch (IOException e) 
		{			
			e.printStackTrace();
			System.out.println("Client-44");
		} 
		catch (ClassNotFoundException e) 
		{
			//e.printStackTrace();
			System.out.println("Clien-47");
		} 
		finally 
		{
			try 
			{	
				out.close();
				in.close();
				socket.close();
			}
			catch (IOException e) 
			{
				//e.printStackTrace();
				System.out.println("Clien-60");
			}
			catch(NullPointerException e)
			{
				System.out.println("null pointer exception");
			}
		}	
		return new Solution(null);
	}
}
